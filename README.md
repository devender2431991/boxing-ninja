# README #

### What is this repository for? ###

* Boxing ninja
* Version 0.0.1


### How do I get set up? ###

* Clone this repo https://devender2431991@bitbucket.org/devender2431991/boxing-ninja.git on your local machine
* Change serialization path in the file application.properties in resources folder. Just directory name only. No file name. 
* This is being used when user want to save game and play later.
* Navigate to project root directoty and Run this command "gradlew clean build". 
* It will download all the required things and will make buils.
* No DB required

* Now after build successful, navigate in the buils folder and then in libs. You will see boxing-ninja-0.0.1-SNAPSHOT.jar

To run the jar use following command from your terminal.

java -jar boxing-ninja-0.0.1-SNAPSHOT.jar

Please make sure that you change serialization path in the file application.properties in resources folder before starting build process.

BoxingNinjaApplication.java is the starting point of this application. Main method reside in this file.

Please see Wiki page for batter understanding of project.
https://bitbucket.org/devender2431991/boxing-ninja/wiki/browse/

###Sample output###:
https://bitbucket.org/devender2431991/boxing-ninja/wiki/edit/Sample%20Output

### TODO for me ###

* Test cases for few classes still pending.

### Who do I talk to? ###

* Email : devender2431991@gmail.com
Skype : devender.kumar785
